import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        String pathName = "ADC";
        File[] filesList;
        File filesPath = new File(pathName);
        String destName = "src/main/ADC";
        File destDir = new File(destName);

        try {
            FileUtils.copyDirectory(filesPath, destDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ChildFile childFile = new ChildFile();
        childFile.renameChild(destDir);

       /*String pathXML = "A:\\ADCPointOfSaleManageSvRQ_LH_A-DXB.xml";

       DOMParser dp = new DOMParser();
       dp.editXML(pathXML);*/
    }
}
