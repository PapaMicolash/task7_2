import java.io.File;
import java.io.IOException;

public class ChildFile {

    public void renameChild(File file) {

        /*if (file.isFile()) {
            if (!file.getParentFile().getName().equals("ADC")) {
                DOMParser dp = new DOMParser();
                dp.editXML(file.getAbsolutePath());
                file.renameTo(new File(file.getParent() + "/PointOfSaleManageSvRQ_ADC_" + file.getParentFile().getName()+".xml") );
                System.out.println(file.getName());
            }
        }

        if (file.isDirectory() ) {
            File[] children = file.listFiles();
            for (File child : children) {
                if (child.getName().equals("PointOfSaleManageSvRQ_ADC.xml")) continue;
                this.renameChild(child);
                child.renameTo(new File(child.getParent() + "/ADC_" + child.getName()));
                System.out.println(file.getName());
            }
        }*/

        File[] children = file.listFiles();
        for (File child : children) {
            if (child.getName().equals("PointOfSaleManageSvRQ_ADC.xml")) continue;
            if (child.isDirectory()) {
                this.renameChild(child);
                child.renameTo(new File(child.getParent() + "/ADC_" + child.getName()));
                System.out.println(child.getName());
            } else if (child.isFile()) {
                DOMParser dp = new DOMParser();
                dp.editXML(child.getAbsolutePath());
                child.renameTo(new File(child.getParent() + "/PointOfSaleManageSvRQ_ADC_" + child.getParentFile().getName()+".xml") );
                System.out.println(child.getName());
            }
        }

    }
}
