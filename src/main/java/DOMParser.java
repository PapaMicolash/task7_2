import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class DOMParser {
    public void editXML(String filePath) {

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filePath);

            //update CreatePointOfSale attributes
            Node pointOfSale = doc.getElementsByTagName("PointOfSale").item(0);
            NamedNodeMap attr = pointOfSale.getAttributes();
            //!!!!!!!!!!!!!!!!!!!!!
            String valueOfParentPointOfSale = attr.getNamedItem("ParentPointOfSale").getTextContent();

            Node nodeParentPointOfSaleAttr = attr.getNamedItem("ParentPointOfSale");
            nodeParentPointOfSaleAttr.setTextContent("ADC_" + valueOfParentPointOfSale);

            String valueOfPointOfSaleCode = attr.getNamedItem("PointOfSaleCode").getTextContent();

            Node nodePointOfSaleCodeAttr = attr.getNamedItem("PointOfSaleCode");
            nodePointOfSaleCodeAttr.setTextContent("ADC_" + valueOfPointOfSaleCode);

            Node pointOfSaleDescription = doc.getElementsByTagName("PointOfSaleDescription").item(0);
            NamedNodeMap attrPointOfSaleDescription = pointOfSaleDescription.getAttributes();

            String valueOfDescriptionAttr = attrPointOfSaleDescription.getNamedItem("Description").getTextContent();

            Node nodeDescription = attrPointOfSaleDescription.getNamedItem("Description");
            nodeDescription.setTextContent("ADC_" + valueOfDescriptionAttr);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filePath));
            transformer.transform(source, result);


            System.out.println("Done");

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }


    }

}
